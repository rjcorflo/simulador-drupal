<?php
module_load_include('inc', 'rj_simulador', 'rj_simulador.forms.admin');

/* *************************************************************************** */
/* *                    PÁGINA DE ADMIN SIMULACIONES                           */
/* *************************************************************************** */
function rj_simulador_simulaciones_usuarios_admin_page() {
  // Creamos el provider
  $provider = FactoryDataProvider::createDataProvider();

  // Lanzamos el GestorSimulaciones y cargamos las Simulaciones pasadas
  $idsSimulaciones = array(
    GestorSimulaciones::SIM_UNO,
    GestorSimulaciones::SIM_DOS,
    GestorSimulaciones::SIM_TRES,
    GestorSimulaciones::SIM_CUATRO,
    GestorSimulaciones::SIM_CINCO
  );

  try {
    $gestorSimulaciones = new GestorSimulaciones($idsSimulaciones);
  } catch (LogicException $le) {
    $renderArray = array('#markup' => '<p>' . $le->getMessage() . '</p>');
    return $renderArray;
  }

  $renderFormArray = drupal_get_form('rjsim_datos_simulaciones_usuarios_admin_form', $gestorSimulaciones);

  // Creamos la tabla a sacar para mostrar las partidas
  // Headers array
  $headers = array(
    array('data' => t('User ID'), 'field' => 'Uid', 'sort' => 'ASC'),
    array('data' => t('User Name'), 'field' => 'Name'),
    array('data' => t('Email')),
    array('data' => t('Created'), 'field' => 'Created'),
    array('data' => t('Last Login'), 'field' => 'LastLogin'),
    array('data' => t('Last Access'), 'field' => 'LastAccess'),
    array('data' => t('Links'))
  );

  $usuarios = $provider->loadAllSimulatorUsers();

  // Getting the current sort and order parameters from the url
  $order = tablesort_get_order($headers);
  $sort = tablesort_get_sort($headers);

  if (isset($order) && isset($sort)) {
    module_load_include('inc', 'rj_simulador', 'includes/sort_functions');
    usort($usuarios, 'orderUserBy' . $order['sql'] . strtoupper($sort));
  }

  $rows = array();
  foreach ($usuarios as $usuario) {
    $rows[] = array(
      array('data' => $usuario->uid),
      array('data' => $usuario->name),
      array('data' => $usuario->mail),
      array(
        'data' => (new DateTime())->setTimestamp($usuario->created)
          ->format("d-m-Y H:i:s")
      ),
      array(
        'data' => (new DateTime())->setTimestamp($usuario->login)
          ->format("d-m-Y H:i:s")
      ),
      array(
        'data' => (new DateTime())->setTimestamp($usuario->access)
          ->format("d-m-Y H:i:s")
      ),
      array('data' => l(t('Show info about simulations'), 'admin/simulaciones_usuarios/' . $usuario->uid . '/simulaciones'))
    );
  }

  $renderArrayTableSimulaciones = NULL;
  $limit = 20;
  $page = pager_default_initialize(count($rows), $limit, 0);
  $offset = $limit * $page;
  $renderArrayTableSimulaciones =
    array(
      array(
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => array_slice($rows, $offset, $limit),
        '#empty' => t('There are no users with simulations in the database.')
      ),
      array(
        '#theme' => 'pager',
        '#element' => 0,
      ),
    );

  $renderArrayFinal = array(
    '#theme' => 'lista_simulaciones_partidas',
    '#main_content' => array($renderFormArray, $renderArrayTableSimulaciones),
  );

  return $renderArrayFinal;
}