<?php

interface SortableInterface {
  public function sortBy($sortField, $sort);
} 