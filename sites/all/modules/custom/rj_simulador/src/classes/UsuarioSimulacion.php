<?php

class UsuarioSimulacion {
  /* @var stdClass $user La entidad usuario */
  private $user;
  /* @var ListaSimulaciones $listaSimulaciones */
  private $listaSimulaciones;

  public function __construct(stdClass $user) {
    $this->setUser($user);
  }

  /**
   * @return stdClass
   */
  private function getUser() {
    return $this->user;
  }

  /**
   * @param stdClass $user
   */
  private function setUser($user) {
    $this->user = $user;
  }

  /**
   * @return ListaSimulaciones
   */
  public function getListaSimulaciones() {
    if (!isset($this->listaSimulaciones)) {
      $provider = FactoryDataProvider::createDataProvider();
      $this->listaSimulaciones = $provider->loadListaSimulacionesByUsuario($this);
    }
    return $this->listaSimulaciones;
  }

  /**
   * @return int El UID del usuario
   */
  public function getUid() {
    return $this->getUser()->uid;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->getUser()->name;
  }

  /**
   * @return string
   */
  public function getMail() {
    return $this->getUser()->mail;
  }

  /**
   * Método que devuelve todas las partidas de un usuario.
   * @return ListaPartidas Lista de todas las partidas de ese usuario de todas las simulaciones.
   */
  public function retrieveAllPartidas() {
    $listaPartidas = new ListaPartidas();

    foreach ($this->getListaSimulaciones() as $simulacion) {
      $listaPartidas->mergeList($simulacion->getListaPartidas());
    }

    return $listaPartidas;
  }

  /**
   * Método que devuelve todas las partidas del usuario para una simulación en concreto.
   * @param int $idSimulation El id de la simulación para la que recuperar las partidas de los usuarios.
   * @return ListaPartidas Lista de todas las partidas de esa simulación..
   */
  public function retrieveAllPartidasByIdSimulacion($idSimulation) {
    $listaPartidas = new ListaPartidas();

    foreach ($this->getListaSimulaciones() as $simulacion) {
      if ($simulacion->getIdSimulacion() == $idSimulation) {
        $listaPartidas->mergeList($simulacion->getListaPartidas());
        break;
      }
    }

    return $listaPartidas;
  }

  /**
   * @return int Número de partidas de este usuario.
   */
  public function countPartidas() {
    $numeroPartidas = 0;

    foreach ($this->getListaSimulaciones() as $simulacion) {
      $numeroPartidas += $simulacion->getListaPartidas()->count();
    }

    return $numeroPartidas;
  }
}